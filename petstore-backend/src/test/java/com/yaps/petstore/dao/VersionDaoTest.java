package com.yaps.petstore.dao;

import static com.yaps.petstore.utils.ConstantUtils.VERSION_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.yaps.petstore.model.Version;

public class VersionDaoTest  extends AbstractBaseDaoTestCase{

	
	
	@Autowired
    private VersionDao versionDao;
    
	
    private Version version = null;
    
    @Before
    public void setUp(){
    	loadVersion();
    }

    
    @After
    public void tearDown(){
    	version = null;
    	versionDao = null;
    }


    @Test
    public void testCreateVersion() throws Exception {
    	versionDao.saveOrUpdate(version);
    	assertTrue("primary key assigned", version.getId() != null);
    }  
    

    @Test
    public void testGetVersion() throws Exception {

        versionDao.saveOrUpdate(version);
        
        Version aVersion = versionDao.get(version.getId());

        assertNotNull(aVersion);
        assertEquals(version,aVersion);
    }   

    
    /**
     * 
     * create an instanciated object. The one declared as private field in the test class
     */   
	private void  loadVersion() {
		version = new Version();
		version.setName(VERSION_NAME);
	}
   
    
}
