package com.yaps.petstore.search;

import static org.junit.Assert.assertNotNull;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.ClusterState;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.cluster.metadata.IndexTemplateMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.hppc.cursors.ObjectCursor;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.node.Node;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;


@TransactionConfiguration(defaultRollback = true)
@ContextConfiguration({ "classpath:/applicationContext-es-test.xml" })
@Transactional  
@RunWith(SpringJUnit4ClassRunner.class)
public class ElasticSearchInstanceIntegrationTest {

	
	Client manualyLoadedClient;
	
	@Autowired
	Client esClient;

	@Autowired
	Client testClient;

	@Autowired
	Node testNode;

	@Autowired
	Client propertiesEsClient;
	
	
	@Before
	public void setUp(){
		manualyLoadedClient = new TransportClient().addTransportAddress(new InetSocketTransportAddress("localhost",9300));
	}
	
	@After
	public void teadDown(){
		manualyLoadedClient.close();
	}
	
	@Test
	public void loadESNodeTest() throws Exception {
		ClusterState cs = manualyLoadedClient.admin().cluster().prepareState().execute().actionGet().getState();
		
		ImmutableOpenMap<String,IndexMetaData> indices = cs.getMetaData().getIndices();
		for (ObjectCursor<String> s : indices.keys()) {
			IndexMetaData imd = indices.get(s.value);
				System.out.println("index-" + imd.getIndex());
				System.out.println("state name - " + imd.getState().name());
				System.out.println("numberOfReplicas - " + imd.getNumberOfReplicas());
				System.out.println("totalNumberOfShards - " + imd.getNumberOfShards());
				System.out.println("version - " + imd.getVersion());
				System.out.println("settings - " + imd.getSettings().getAsMap().size());
		}
		
		ImmutableOpenMap<String,IndexTemplateMetaData> templates = cs.getMetaData().getTemplates();
		for (ObjectCursor<String> s : templates.keys()) {
			IndexTemplateMetaData imd = templates.get(s.value);
				System.out.println(imd.getTemplate());
		}		
	}
	
	@Test
	public void loadESSpringConfNodeTest() throws Exception {
		ClusterState cs = esClient.admin().cluster().prepareState().execute().actionGet().getState();
		
		
		ImmutableOpenMap<String,IndexMetaData> indices = cs.getMetaData().getIndices();
		for (ObjectCursor<String> s : indices.keys()) {
			IndexMetaData imd = indices.get(s.value);
				System.out.println("index-" + imd.getIndex());
				System.out.println("state name - " + imd.getState().name());
				System.out.println("numberOfReplicas - " + imd.getNumberOfReplicas());
				System.out.println("totalNumberOfShards - " + imd.getNumberOfShards());
				System.out.println("version - " + imd.getVersion());
				System.out.println("settings - " + imd.getSettings().getAsMap().size());
		}
		
		ImmutableOpenMap<String,IndexTemplateMetaData> templates = cs.getMetaData().getTemplates();
		for (ObjectCursor<String> s : templates.keys()) {
			IndexTemplateMetaData imd = templates.get(s.value);
				System.out.println(imd.getTemplate());
		}		
	}
	
	@Test
	public void loadNodeTest() throws Exception {
		assertNotNull(testNode);
		testNode.start();
		
		ClusterState cs = testClient.admin().cluster().prepareState().execute().actionGet().getState();		
		
		ImmutableOpenMap<String,IndexMetaData> indices = cs.getMetaData().getIndices();
		for (ObjectCursor<String> s : indices.keys()) {
			IndexMetaData imd = indices.get(s.value);
				System.out.println("index-" + imd.getIndex());
				System.out.println("state name - " + imd.getState().name());
				System.out.println("numberOfReplicas - " + imd.getNumberOfReplicas());
				System.out.println("totalNumberOfShards - " + imd.getNumberOfShards());
				System.out.println("version - " + imd.getVersion());
				System.out.println("settings - " + imd.getSettings().getAsMap().size());
		}		
		
		testNode.stop();
	}	


	@Test
	public void loadPropertiesESClientTest() throws Exception {
		
		ClusterState cs = propertiesEsClient.admin().cluster().prepareState().execute().actionGet().getState();
		
		
		ImmutableOpenMap<String,IndexMetaData> indices = cs.getMetaData().getIndices();
		for (ObjectCursor<String> s : indices.keys()) {
			IndexMetaData imd = indices.get(s.value);
				System.out.println("index-" + imd.getIndex());
				System.out.println("state name - " + imd.getState().name());
				System.out.println("numberOfReplicas - " + imd.getNumberOfReplicas());
				System.out.println("totalNumberOfShards - " + imd.getNumberOfShards());
				System.out.println("version - " + imd.getVersion());
				System.out.println("settings - " + imd.getSettings().getAsMap().size());
		}
		
		ImmutableOpenMap<String,IndexTemplateMetaData> templates = cs.getMetaData().getTemplates();
		for (ObjectCursor<String> s : templates.keys()) {
			IndexTemplateMetaData imd = templates.get(s.value);
				System.out.println(imd.getTemplate());
		}		
	}	
	
	
}
