package com.yaps.petstore.dao;

import com.yaps.petstore.model.Version;


public interface VersionDao extends GenericDao<Version, Long> {

}
