package com.yaps.petstore.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.yaps.petstore.dao.VersionDao;
import com.yaps.petstore.model.Version;


/**
 * 
 * @author zou
 *
 */

@Repository
public class VersionDaoHibernate extends AbstractGenericDaoHibernate<Version,Long> implements VersionDao {

}
