package com.yaps.petstore.service.supervision.impl;

import java.util.List;

import org.dozer.Mapper;
import org.elasticsearch.client.Client;
import org.elasticsearch.cluster.ClusterState;
import org.elasticsearch.cluster.metadata.IndexMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.hppc.cursors.ObjectCursor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yaps.petstore.common.exception.CheckException;
import com.yaps.petstore.dao.VersionDao;
import com.yaps.petstore.model.Version;
import com.yaps.petstore.model.supervision.ESVersionDTO;
import com.yaps.petstore.model.supervision.VersionDTO;
import com.yaps.petstore.service.supervision.SupervisionService;

@Service
@Transactional
public class SupervisionServiceImpl implements SupervisionService {

	
	public static final String VERSION_INVALID_ID = "Invalid id";
	
	public static final String UNCORRECT_VERSIONS = "More than one line in version table";
	
	@Autowired
	protected VersionDao versionDao;
	
	@Autowired
	protected Client esClient;
	
	@Autowired
	protected Mapper mapper;
	
	
	public VersionDao getVersionDao(){
		return this.versionDao;
	}
	
	public void setVersionDao(VersionDao aVersionDao){
		this.versionDao = aVersionDao;
	}
	

	public Mapper getMapper() {
		return mapper;
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}	
	
	
	public Client getEsClient() {
		return esClient;
	}

	public void setEsClient(Client esClient) {
		this.esClient = esClient;
	}
	

	/* (non-Javadoc)
	 * @see com.yaps.petstore.service.supervision.impl.SupervisionService#findVersion(java.lang.Long)
	 */
	@Transactional(readOnly=true)
	public VersionDTO findVersion(Long versionId) throws CheckException {
		if (versionId == null){
            throw new CheckException(VERSION_INVALID_ID);			
		}


		Version version = versionDao.get(versionId) ;
        VersionDTO versionWorkDto = mapper.map(version, VersionDTO.class);
        return versionWorkDto;		
	}

	/*
	 * (non-Javadoc)
	 * @see com.yaps.petstore.service.supervision.SupervisionService#findVersion()
	 */
	@Transactional(readOnly=true)
	public VersionDTO findVersion() throws CheckException {
		List<Version> all = versionDao.findAll();
		
		if (all == null || all.size() != 1) {
			throw new CheckException(UNCORRECT_VERSIONS);
		}
		
        VersionDTO versionWorkDto = mapper.map(all.get(0), VersionDTO.class);
        return versionWorkDto;
	}

	
	/*
	 * (non-Javadoc)
	 * @see com.yaps.petstore.service.supervision.SupervisionService#elasticSearchVersion()
	 */
	@Override
	public ESVersionDTO elasticSearchVersion() throws CheckException {
		
		ClusterState cs = esClient.admin().cluster().prepareState().get().getState();
		
		ESVersionDTO esVersionDTO = new ESVersionDTO();
		
		ImmutableOpenMap<String,IndexMetaData> indices = cs.getMetaData().getIndices();
		for (ObjectCursor<String> s : indices.keys()) {
			IndexMetaData imd = indices.get(s.value);

			esVersionDTO.setIndex(imd.getIndex());
			esVersionDTO.setStateName(imd.getState().name());
			esVersionDTO.setNumberOfReplicas(imd.getNumberOfReplicas());
			esVersionDTO.setNumberOfShards(imd.getNumberOfShards());
			esVersionDTO.setVersion(imd.getVersion());
		}
		
		return esVersionDTO;
	}
	
	
}
