package com.yaps.petstore.cache;

import net.spy.memcached.MemcachedClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.yaps.petstore.tech.log.SFFilter;

@Component
public class ServiceCacheImpl implements ServiceCache {

	Logger logger = LoggerFactory.getLogger(ServiceCacheImpl.class.getName());

	@Autowired
	@Qualifier("memcachedClient")
	protected MemcachedClient memcachedClient;

	@Value("#{appProperties['memcached.ttl']}")
	int ttl;
	
	/* (non-Javadoc)
	 * @see com.yaps.petstore.cache.ServiceCache#get(java.lang.String)
	 */
	@Override
	public <T> T  get(String key){
		T result = null;
		
		try{
			Object tmpObj = memcachedClient.get(key);
			
			if (tmpObj != null) {
				result = (T) tmpObj;
			}			
		}catch(Exception e){
			logger.error("{} - message {}", SFFilter.SYSTEM_FAILURE_ERROR,e.getMessage());
		}
	
		return result;
	}
	
	
	/* (non-Javadoc)
	 * @see com.yaps.petstore.cache.ServiceCache#set(java.lang.String, java.lang.Object)
	 */
	
	@Override
	public void set(String key, Object o){
		try{
			memcachedClient.set(key, ttl, o);			
		}catch(Exception e){
			logger.error("{} - message {}"
					, SFFilter.SYSTEM_FAILURE_ERROR
					,e.getMessage());
		}		
	}
	
	
}
