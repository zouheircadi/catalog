package com.yaps.petstore.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yaps.petstore.common.exception.CheckException;
import com.yaps.petstore.model.CategoryDTO;
import com.yaps.petstore.model.ItemDTO;
import com.yaps.petstore.model.ProductDTO;
import com.yaps.petstore.service.catalog.CatalogService;

@Controller
@RequestMapping("/catalog")
public class CatalogController {

	Logger logger = LoggerFactory.getLogger(CatalogController.class.getName());	
	
	@Autowired
	CatalogService catalogService;
	
	
	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public @ResponseBody List<CategoryDTO> getCategories() {
 
		return catalogService.findCategories();
 
	}

	@RequestMapping(value = "/category/{categoryId}", method = RequestMethod.GET)
	public @ResponseBody CategoryDTO getCategory(@PathVariable Long categoryId) throws CheckException {
		
		return catalogService.findCategory(categoryId);
	
	}	

	
	@RequestMapping(value = "/category/add", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public CategoryDTO addCategory(@RequestBody final CategoryDTO categoryDto) throws CheckException {
		logger.info("start addCategory");

		logger.debug("categoryDto  = {}", categoryDto.toString());

        return catalogService.createCategory(categoryDto);
	}
	
	
	@RequestMapping(value = "/category/update/{categoryId}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public CategoryDTO updateCategory(@PathVariable(value = "categoryId") Long categoryId, @RequestBody final CategoryDTO categoryDto) throws CheckException {
		logger.info("start updateCategory");

		logger.debug("categoryDto  = {}", categoryDto.toString());
		
		catalogService.updateCategory(categoryDto);
		return catalogService.findCategory(categoryId);
	}
	
	
	@RequestMapping(value = "/category/delete/{categoryId}", method = RequestMethod.DELETE, consumes = "application/json")
	@ResponseBody
	public void deleteCategory(@PathVariable(value = "categoryId") Long categoryId) throws CheckException {
		logger.info("start deleteCategory");

		logger.debug("id to delete  = {}", categoryId);
		
		catalogService.deleteCategory(categoryId);
		
		
	}	
	
		
	
	@RequestMapping(value = "/products/{categoryId}", method = RequestMethod.GET)
	public @ResponseBody List<ProductDTO> getProductsByCategory(@PathVariable Long categoryId) throws CheckException {
 
		return catalogService.findProducts(categoryId);
 
	}

	
	@RequestMapping(value = "/product/{productId}", method = RequestMethod.GET)
	public @ResponseBody ProductDTO getProduct(@PathVariable Long productId) throws CheckException {
		
		return catalogService.findProduct(productId);
	
	}
	
	
	@RequestMapping(value = "/product/add", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public ProductDTO addProduct(@RequestBody final ProductDTO productDto) throws CheckException {
		logger.info("start productDto");

		logger.debug("productDto  = {}", productDto.toString());

        return catalogService.createProduct(productDto);
	}
	
	
	@RequestMapping(value = "/product/update/{productId}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public ProductDTO updateProduct(@PathVariable(value = "productId") Long productId, @RequestBody final ProductDTO productDto) throws CheckException {
		logger.info("start updateCategory");

		logger.debug("productDto  = {}", productDto.toString());
		
		catalogService.updateProduct(productDto);
		return catalogService.findProduct(productId);
	}
	
	
	@RequestMapping(value = "/product/delete/{productId}", method = RequestMethod.DELETE, consumes = "application/json")
	@ResponseBody
	public void deleteProduct(@PathVariable(value = "productId") Long productId) throws CheckException {
		logger.info("start deleteProduct");

		logger.debug("id to delete  = {}", productId);
		
		catalogService.deleteProduct(productId);
		
		
	}	

	
	
	@RequestMapping(value = "/items/{productId}", method = RequestMethod.GET)
	public @ResponseBody List<ItemDTO> getItemsByProductId(@PathVariable Long productId) throws CheckException {
		
		return catalogService.findItems(productId); 
	
	}

	
	@RequestMapping(value = "/item/{itemId}", method = RequestMethod.GET)
	public @ResponseBody ItemDTO getItem(@PathVariable Long itemId) throws CheckException {
		
		return catalogService.findItem(itemId);
	
	}
	

	@RequestMapping(value = "/item/add", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public ItemDTO addItem(@RequestBody final ItemDTO itemDto) throws CheckException {
		logger.info("start addItem");

		logger.debug("itemDto  = {}", itemDto.toString());

        return catalogService.createItem(itemDto);
	}
	
	
	@RequestMapping(value = "/item/update/{itemId}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public ItemDTO updateItem(@PathVariable(value = "itemId") Long itemId, @RequestBody final ItemDTO itemDto) throws CheckException {
		logger.info("start updateItem");

		logger.debug("itemDto  = {}", itemDto.toString());
		
		catalogService.updateItem(itemDto);
		return catalogService.findItem(itemId);
	}
	
	
	@RequestMapping(value = "/item/delete/{itemId}", method = RequestMethod.DELETE, consumes = "application/json")
	@ResponseBody
	public void deleteItem(@PathVariable(value = "itemId") Long itemId) throws CheckException {
		logger.info("start itemCategory");

		logger.debug("id to delete  = {}", itemId);
		
		catalogService.deleteItem(itemId);
		
		
	}	
	
	
		
}
