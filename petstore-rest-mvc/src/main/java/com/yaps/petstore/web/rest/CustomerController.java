package com.yaps.petstore.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yaps.petstore.common.exception.CheckException;
import com.yaps.petstore.model.CustomerDTO;
import com.yaps.petstore.service.customer.CustomerService;

@Controller
@RequestMapping("/customer-management")
public class CustomerController {

	Logger logger = LoggerFactory.getLogger(CustomerController.class.getName());	
	
	@Autowired
	CustomerService customerService;
	

	@RequestMapping(value = "/customers", method = RequestMethod.GET)
	public @ResponseBody List<CustomerDTO> getCustomers() throws CheckException {
		return customerService.findCustomers();
	}
	
	
	@RequestMapping(value = "/customer/{customerId}", method = RequestMethod.GET)
	public @ResponseBody CustomerDTO getCustomer(@PathVariable Long customerId) throws CheckException {
		return customerService.findCustomer(customerId);
	}
	

	@RequestMapping(value = "/customer/add", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public CustomerDTO addCustomer(@RequestBody final CustomerDTO customerDto) throws CheckException {
		logger.info("start addCustomer");

		logger.debug("customerDto  = {}", customerDto.toString());

        return customerService.createCustomer(customerDto);
	}
	
	
	@RequestMapping(value = "/customer/update/{customerId}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public CustomerDTO updateCustomer(@PathVariable(value = "customerId") Long customerId, @RequestBody final CustomerDTO customerDto) throws CheckException {
		logger.info("start updateCustomer");

		logger.debug("customerDto  = {}", customerDto.toString());
		
		customerService.updateCustomer(customerDto);
		return customerService.findCustomer(customerId);
	}
	
	
	@RequestMapping(value = "/customer/delete/{customerId}", method = RequestMethod.DELETE, consumes = "application/json")
	@ResponseBody
	public void deleteCustomer(@PathVariable(value = "customerId") Long customerId) throws CheckException {
		logger.info("start deleteCustomer");

		logger.debug("id to delete  = {}", customerId);
		
		customerService.deleteCustomer(customerId);
		
	}	

	
}

