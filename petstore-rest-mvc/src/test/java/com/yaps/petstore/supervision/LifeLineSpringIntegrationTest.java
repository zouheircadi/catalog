package com.yaps.petstore.supervision;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.servlet.ServletContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.yaps.petstore.web.rest.supervision.LifeLine;
import com.yaps.petstore.web.rest.supervision.SERVER_STATUS;

@ContextConfiguration({ "classpath:/applicationContext-configuration-test.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class LifeLineSpringIntegrationTest  {

	@Autowired
	LifeLine lifeline;

	ServletContext context;
	
	
	@Test
	public void testAdminVersion() throws Exception {
		
		assertNotNull(lifeline);

		lifeline.adminVersion();

		assertNotNull(lifeline.getVersion());
		assertEquals("1.0-SNAPSHOT", lifeline.getVersion());
	}

	
	@Test
	public void testCheckInstance() throws Exception {
		
		assertNotNull(lifeline);

		ResponseEntity<String> result =  lifeline.checkInstance();
		

		assertNotNull(lifeline.getInstance());
		assertEquals(SERVER_STATUS.OK.toString(), result.getBody());
	}
	
	@Test
	public void testCheckDependencies() throws Exception {
		
		assertNotNull(lifeline);
		
		String result =  lifeline.checkDependencies();
		

		assertTrue(result.contains("<ul>"));
	}


}
