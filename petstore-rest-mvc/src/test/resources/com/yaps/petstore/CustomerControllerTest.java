package com.yaps.petstore;

import com.yaps.petstore.model.CustomerDTO;
import com.yaps.petstore.service.customer.CustomerService;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created with IntelliJ IDEA.
 * User: alexandre_godet
 * Date: 23/02/2014
 * Time: 18:43
 * To change this template use File | Settings | File Templates.
 */
public class CustomerControllerTest {

    @Ignore
    @Test
    public void testAddCustomer() throws Exception {
        CustomerController customerController = new CustomerController();
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(32L);

        customerController.customerService = mock(CustomerService.class);

        when(customerController.customerService.createCustomer(customerDTO)).thenReturn(customerDTO);
        when(customerController.customerService.findCustomer(customerDTO.getId())).thenReturn(customerDTO);

        CustomerDTO returnCustomer = customerController.addCustomer(customerDTO);

        verify(customerController.customerService).createCustomer(customerDTO);
        verify(customerController.customerService).findCustomer(32L);
        assertEquals(32L,returnCustomer.getId().longValue());

    }
}
