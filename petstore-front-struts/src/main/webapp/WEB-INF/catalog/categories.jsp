<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<P><strong>All categories</strong></P>

<table>
	<s:iterator value="categoryLst">
	
	    <tr>
	        <td><s:property value="id" /></td> 
	        <td>
				<s:url id="productUrl" action="products" method="findProducts">
					<s:param name="categoryId">
						<s:property value="id" />
					</s:param>
				</s:url>
				<s:a href="%{productUrl}"><s:property value="name" /></s:a>	<br/>
	        </td>
	        <td><s:property value="description" /></td>	        
	    </tr>
	
	</s:iterator>
</table>
