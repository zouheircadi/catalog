<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<P><strong>Products for this Category 1</strong></P>
<table>
<s:iterator value="productLst">

    <tr>
        <td><s:property value="id" /></td> 
        <td></td>
        <td>
			<s:url id="itemsUrl" action="items" method="findItems">
				<s:param name="productId">
					<s:property value="id" />
				</s:param>
			</s:url>
			<s:a href="%{itemsUrl}"><s:property value="name" /></s:a>	<br/>
        </td>
	    <td><s:property value="description" /></td>
    </tr>

</s:iterator>
</table>