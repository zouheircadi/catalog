<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<P><strong>Item</strong></P>


	<table>
	    <tr>
	        <td>
	        	<s:property value="itemDto.id" />
	        </td> 
	    </tr>
	    <tr>
	        <td>
	        	<s:property value="itemDto.product.name" />
	        </td> 
	    </tr>
	    <tr>
	        <td>
	        	<s:property value="itemDto.name" />
	        </td> 
	    </tr>
	    <tr>
	        <td>
	        	<s:property value="itemDto.unitCost" />
	        </td> 
	    </tr>
	    <tr>
	        <td>
	        	<img src="<%= request.getContextPath() %>/images/${itemDto.imagePath}"/>
	        </td> 
	    </tr>

		<s:if test="#session.customer != null">
				<s:url id="shoppingCartUrl" action="shopping-cart-add" method="addItem">
					<s:param name="itemId">
						<s:property value="itemId" />
					</s:param>
				</s:url>
	    			<tr>
						<td colspan=2><s:a href="%{shoppingCartUrl}">Add to Cart</s:a></td>
					</tr>		
		</s:if>	    
    </table>