<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<P><strong>Items for this product</strong></P>
<table>
<s:iterator value="itemLst">


    <tr>
        <td>
        	<s:property value="id" />
        	
			<s:url id="itemUrl" action="item" method="findItem">
				<s:param name="itemId">
					<s:property value="id" />
				</s:param>
			</s:url>
			<s:a href="%{itemUrl}"><s:property value="name" /></s:a>	<br/>        	
        </td> 
    </tr>
</s:iterator>
</table>