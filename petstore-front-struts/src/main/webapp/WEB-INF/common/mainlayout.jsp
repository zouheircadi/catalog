<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<html>
	<head>
		<title><tiles:insertAttribute name="title" flush=""/></title>
	</head>
	<link rel="stylesheet" href="./css/style_1.css" type="text/css" width="100%">
	<body>
	<TABLE cellSpacing=0  cellpadding="5" width="100%">
	<%--HEADER--%>
			<tr>
				<td colspan="3">
					<tiles:insertAttribute name="header" />
				</td>
			</tr>
			<%--NAVIGATION --%>
			<tr>

				<%--CENTRAL BODY--%>

				<td align="center" >
    				<tiles:insertAttribute name="body" />
    			</td>

				<td></td>
			</tr>


			<%--FOOTER--%>
    		<tr>
    			<td colspan="3">
    				<tiles:insertAttribute name="footer" />
    			</td>
    		</tr>
		</table>
	</body>
</html>
