<%@ taglib prefix="s" uri="/struts-tags" %>


			<s:if test="%{orders != null }">
				Customer orders
				<table>
                <s:iterator value="orders" id="order" >
					<tr>
						<td>Your order id : </td>
						<td><s:property value="id"/></td>
					</tr>
					<tr>
						<td colspan="2">
						<table>
						 	<s:iterator value="orderLines" id="orderline" >
						 		<tr>
									<td>id</td>
									<td><s:property value="id"/></td>									
						 		</tr>
						 		<tr>
									<td>quantity</td>
									<td><s:property value="quantity"/></td>									
						 		</tr>						 		
						 	</s:iterator>
						</table>
						<td>
					</tr>
                </s:iterator>
                </table>				
			</s:if>

			<s:form  method="POST"  >
				<table style="margin-left: 50px" border="1" cellspacing="0" cellpadding="0" >

					<s:set name="myId" value="%{customerDto.id}"/>
					<tr>
						<TD align=middle bgColor=#336666>
							<FONT color=#ffffff>
								<strong>
									<s:if test="%{#myId == null || #myId == ''}">
										Create customer
									</s:if>
									<s:else>
										Update customer
									</s:else>

									
								</strong>
							</FONT>
						</TD>
					</tr>
					<tr><td bgColor=#336666 >
							<table  cellSpacing=1 cellPadding=0 width="100%" border=0 bgcolor="#ffffff">

								<tr>
									<td><br/><div class="form_parag">Personal information</div></td>
									<td></td>
								</tr>
								<tr>
									<td>* Customer Id</td>
	                    			<td>${customerDto.id}</td>
	                    			<s:hidden name="id"  label="%{customerDto.id}"  value="%{customerDto.id}"/>
	                    			<s:hidden name="password" label="#request.pass" value="%{customerDto.password}" />
	                    			<s:hidden name="password2" label="#request.pass2"/>
	                    				                    												
								</tr>
								<tr>
									<td>* First name</td>
									<td><s:textfield  name="firstname"  value="%{customerDto.firstname}"/></td>
								</tr>
								<tr>
									<td>* Last name</td>
							 		<td><s:textfield  name="lastname" value="%{customerDto.lastname}"/></td>
								</tr>

								<tr>
									<td>* email(identifier)</td>
									<s:if test="#request.emailId != null">
										<td><s:textfield name="email"  value="%{#request.emailId}"  /></td>
									</s:if>
									<s:else>
										<td><s:textfield name="email"  value="%{customerDto.email}"  /></td>
									</s:else>								
								</tr>								

								<tr>
									<td>telephone</td>
									<td><s:textfield name="telephone"  value="%{customerDto.telephone}"/></td>
								</tr>
								<tr>
									<td><br/><div class="form_parag">Address</div></td>
									<td></td>
								</tr>
								<tr>
									<td>Street 1</td>
									<td><s:textfield  name="address.street1"  value="%{customerDto.address.street1}" /></td>
								</tr>
								<tr>
									<td>Street 2</td>
									<td><s:textfield name="address.street2" value="%{customerDto.address.street2}"/></td>
								</tr>
								<tr>
									<td>City</td>
									<td><s:textfield name="address.city" value="%{customerDto.address.city}"/></td>
								</tr>
								<tr>
									<td>State</td>
									<td><s:textfield  name="address.state"  value="%{customerDto.address.state}"/></td>									
								</tr>
								<tr>
									<td>Zipcode</td>
									<td><s:textfield name="address.zipcode" value="%{customerDto.address.zipcode}"/></td>
								</tr>
								<tr>
									<td>Country</td>
									<td><s:textfield name="address.country"  value="%{customerDto.address.country}"/></td>
								</tr>
								<%-- 
								<tr>
									<td><br/><div class="form_parag">Credit card information</div></td>
									<td></td>
								</tr>
								<tr>
									<td>Type</td>
									<td><INPUT TYPE="text" NAME="creditcard.creditCardType"/></td>
								</tr>
								<tr>
									<td>Number</td>
									<td><INPUT TYPE="text" NAME="creditcard.creditCardNumber"/></td>
								</tr>
								<tr>
									<td>Expire date (MM/YY)</td>
									<td><INPUT TYPE="text" NAME="creditcard.creditCardExpiryDate"/></td>
								</tr>
								--%>
								<tr>
									<s:if test="%{#myId == null || #myId == ''}">
										<td><br/>create : <s:submit  value="submit" action="customer-create" method="create" label="create"/></td>
									</s:if>
									<s:else>
										<td><br/>update : <s:submit  value="submit" action="customer-update" method="update" label="update" /></td>
									</s:else>									
									
									<td><br/><s:submit  value="cancel" action="welcome" method="findCategories"/></td>									 
								</tr>
							</table>
						</td></tr>
					</table>
				</s:form>

			<!-- fin formulaire-->
