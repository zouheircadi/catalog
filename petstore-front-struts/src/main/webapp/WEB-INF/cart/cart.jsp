<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<P><strong>Item</strong></P>

        <P><strong>Shopping Cart</strong></P>

        <!-- 
		<s:if test="#session.customer != null">
				<s:url id="shoppingCartUrl" action="shopping-cart-add" method="addItemToCart">
					<s:param name="itemDto.id">
						<s:property value="itemId" />
					</s:param>
				</s:url>
	    			<tr>
						<td colspan=2><s:a href="%{shoppingCartUrl}">Add to Cart</s:a></td>
					</tr>		
		</s:if>
		 -->        
        
        
            <%-- Shopping Cart is empty --%>
            <s:if test="#session.cart == null">
                <P><strong>The Shopping Cart is empty</strong></P>
            </s:if>

            <%-- There are items in the Shopping Cart --%>
            <s:else>
                <TABLE cellSpacing=1 cellPadding=1 width="100%" border=1>
                    <TR>
                        <TD>
                            <TABLE >

                                <%-- Lists all the items in the shopping cart --%>
                                <s:iterator value="finalCartDto.cartItems" id="cartItemDTO" >
                                    <s:form  method="POST" theme="simple">
                                        <tr valign="top" >
                                            <td>
												<s:url id="findItemUrl" action="item" method="findItem">
													<s:param name="itemId">
														<s:property value="#cartItemDTO.itemId" />
													</s:param>
												</s:url>
												<s:a href="%{findItemUrl}"><s:property value="#cartItemDTO.productDescription" /></s:a><br/>												
												
                                            </td>
                                            <td>
												<s:url id="removeItemUrl" action="shopping-cart-remove" method="removeItem">
													<s:param name="itemId">
														<s:property value="#cartItemDTO.itemId" />
													</s:param>
												</s:url>
												<s:a href="%{removeItemUrl}">remove</s:a>                                           
                                            
                                            </td>
                                            <td bgcolor="#FF0000">
                                            	<s:textfield  name="quantity" value="%{#cartItemDTO.quantity}" />
                                            	<s:hidden name="itemId"  label="%{shoppingCartItem.itemId}"  value="%{#cartItemDTO.itemId}"/> 
                                            </td>
                                            <td>
								               <s:submit  value="update" action="shopping-cart-update" method="updateItemQuantity"/>
                                            </td>
                                            <td>
                                            	<s:property value="#cartItemDTO.unitCost"/>
                                            </td>
                                            <td>
                                                =
                                            </td>
                                            <td>
                                            	<s:property value="#cartItemDTO.totalCost"/>                                            
                                            </td>
                                        </tr>
                                    </s:form>
                                </s:iterator>

                                <%-- Total --%>
                                <tr>
                                    <td colspan="7" align="right">
                                        <b>Total:</b>
                                    </td>
                                    <td bgcolor="#CCCCFF" align="right">
                                        <s:property value="%{finalCartDto.total}"/>
                                    </td>
                                </tr>
                            </table>
                        </TD>
                    </TR>
                </table>
                
                <div align="left">
					<s:url id="checkoutUrl" action="shopping-cart-check-out" method="checkOut"/>
					<s:a href="%{checkoutUrl}">Check Out</s:a>                
                </div>
            </s:else>
