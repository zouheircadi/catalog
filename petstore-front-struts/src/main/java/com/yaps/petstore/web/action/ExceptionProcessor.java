package com.yaps.petstore.web.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.xwork2.ActionSupport;

public class ExceptionProcessor extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1337645178243034723L;

	Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	private Exception exception;

	@Override
	public String execute() {
		logger.error("Is exception null: {}", (exception == null));
		logger.error("message {}", exception.getMessage());
		return "error";
	}

	public void setException(Exception exceptionHolder) {
		this.exception = exceptionHolder;
	}

	public Exception getException() {
		return exception;
	}

}
