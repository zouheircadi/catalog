package com.yaps.petstore.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.yaps.petstore.model.CustomerDTO;
import static   com.yaps.petstore.web.utils.SessionUtils.CUSTOMER_SESSION;;

public class LoginInterceptor implements Interceptor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 899431365403759098L;
	
	
	Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	public LoginInterceptor() {
	}

	public void destroy() {

	}

	public void init() {

	}

	public String intercept(ActionInvocation actionInvocation) throws Exception {

	Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
    final ActionContext context = actionInvocation.getInvocationContext();
    HttpServletRequest request = (HttpServletRequest) context.get(ServletActionContext.HTTP_REQUEST);
    HttpSession session = request.getSession(true);

    
    

    try {

        CustomerDTO aCustomerDto = (CustomerDTO) session.getAttribute(CUSTOMER_SESSION);

        if (aCustomerDto == null) {
            return "login";
        }

    } catch (Exception e) {
        logger.error("message", e);
    }

    //Invoke action
    String result = actionInvocation.invoke();

    return result;
}
}